interface MealBuilder{
    fun buildFrenchFries()
    fun buildDrink()
    fun buildHamburger()
    fun buildCheese()
}

class HamburgerBuilder: MealBuilder {
    private var hamburger = Hamburger()

    fun reset() {
        hamburger = Hamburger()
    }

    override fun buildFrenchFries() {
        hamburger.add("French Fries")
    }

    override fun buildDrink() {
        hamburger.add("Coke Drink")
    }

    override fun buildHamburger() {
        hamburger.add("Hamburger")
    }

    override fun buildCheese() {
        hamburger.add("Cheddar Cheese")
    }

    fun retrieveHamburger(): Hamburger {
        var result = this.hamburger
        reset()
        return result
    }
}

class HamburgerDirector{
    private var hamburgerBuilder: MealBuilder? = null

    fun update(mealBuilder: MealBuilder ){
        this.hamburgerBuilder  = mealBuilder
    }

    fun buildSimpleHamburger(){
        hamburgerBuilder?.buildHamburger()
        hamburgerBuilder?.buildCheese()
    }

    fun buildFullHamburger(){
        hamburgerBuilder?.buildFrenchFries ()
        hamburgerBuilder?.buildDrink ()
        hamburgerBuilder?.buildHamburger ()
        hamburgerBuilder?.buildCheese ()
    }
}

class Hamburger{
    private var parts = mutableListOf<String>()
    fun add(part: String ){
        this.parts.add(part)
    }
    
    fun listParts() : String { 
        return "Hamburger parts: "  + parts.joinToString(", " ) + "n"
    }
}

fun main(){
    val hamburgerDirector = HamburgerDirector ()
    val hamburgerBuilder = HamburgerBuilder ()
    hamburgerDirector.update(hamburgerBuilder)
    print("Simple hamburger: ")
    hamburgerDirector.buildSimpleHamburger()
    println(hamburgerBuilder.retrieveHamburger().listParts())

    print("Full Hamburger: ")
    hamburgerDirector.buildFullHamburger()
    println(hamburgerBuilder.retrieveHamburger().listParts())
}