class AccountManager private constructor(){
    val userInfo = ("erika")
    var isAuthenticated = false

    fun authenticateUser(){
        if (isAuthenticated) {
            println("Authenticated user")
            return
        }else{
            println("Unauthenticated user")
            isAuthenticated = true
        }
    }

    init{
        println("I'm created")
    }

    companion object {
        private val sharedInstance: AccountManager = AccountManager()

        @Synchronized
        fun getInstance(): AccountManager{
            return sharedInstance
        }
    }
}

fun main() {
    println(AccountManager.getInstance().userInfo)
    AccountManager.getInstance().authenticateUser()
}

