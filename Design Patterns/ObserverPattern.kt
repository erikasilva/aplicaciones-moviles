interface Observer{
    var id: Int?
    fun update()
}

class Subject{
    private var observerArray: MutableList<Observer> = ArrayList()
    var number = 0
        get() = field
        set(newValue) {
            field = newValue
            notifyEvent()
        }

    fun attachObserver(observer : Observer){
        observerArray.add(observer)
    }

    private fun notifyEvent(){
        for (observer in observerArray){
            observer.update()
        }
    }
}

class BinaryObserver : Observer {
    private var subject = Subject()
    override var id: Int?

    constructor(subject: Subject, id: Int){
        this.subject = subject
        this.subject.attachObserver(this)
        this.id = id
    }
    
    override fun update(){
        println("Binary: ${java.lang.Integer.toBinaryString(subject.number)}")
    }
}

class HexaObserver : Observer {
    private var subject = Subject()
    override var id: Int?

    constructor(subject : Subject, id : Int) {
        this.subject = subject
        this.subject.attachObserver(this)
        this.id = id
    }

    override fun update(){
        println("Hex: ${java.lang.Integer.toHexString(subject.number)}")
    }
}

class OctalObserver : Observer{
    private var subject = Subject()
    override var id: Int?

    constructor(subject : Subject, id : Int) {
        this.subject = subject
        this.subject.attachObserver(this)
        this.id = id
    }

    override fun update(){
        println("Octal: ${java.lang.Integer.toOctalString(subject.number)}")
    }

}

fun main(){
    val subject = Subject()

    val binary = BinaryObserver(subject, 1)
    val octal = OctalObserver(subject, 2)
    val hex = HexaObserver(subject, 3)

    subject.number = 15
    subject.number = 2
}