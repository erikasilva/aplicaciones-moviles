interface Transporting {
    fun getSpeed(): Double
    fun getTraction(): Double
}

// Core Component
class RaceCar: Transporting{
    private val speed = 10.0
    private val traction = 10.0

    override fun getSpeed(): Double{
        return speed
    }

    override fun getTraction(): Double{
        return traction
    }
}

//Abstract Decorator
class TireDecorator: Transporting{
    private val transportable: Transporting

    constructor(transportable: Transporting){
        this.transportable = transportable
    }

    override fun getSpeed(): Double {
        return transportable.getSpeed()
    }

    override fun getTraction(): Double {
        return transportable.getTraction()
    }
}

class OffRoadTireDecorator: Transporting {
    private val transportable: Transporting

    constructor(transportable: Transporting) {
        this.transportable = transportable
    }

    override fun getSpeed(): Double{
        return transportable.getSpeed() - 3.0
    }

    override fun getTraction(): Double{
        return transportable.getTraction() + 3.0
    }
}

//Decorating Decorators
class ChainedTireDecorator: Transporting {
    private val transportable: Transporting

    constructor(transportable: Transporting) {
        this.transportable = transportable
    }

    override fun getSpeed(): Double {
        return transportable.getSpeed() - 1.0
    }

    override fun getTraction(): Double {
        return transportable.getTraction() * 1.1
    }
}

fun main() {
// Create Race Car
    val defaultRaceCar = RaceCar()
    println("Speed of Default Race Car: ${defaultRaceCar.getSpeed()}")
    println("Traction of Default Race Car: ${defaultRaceCar.getTraction()}")

// Modify Race Car
    val offRoadRaceCar = OffRoadTireDecorator(defaultRaceCar)
    println("Speed of Off Road Tire Decorator: ${offRoadRaceCar.getSpeed()}")
    println("Traction of Off Road Tire Decorator: ${offRoadRaceCar.getTraction()}")

// Double Decorators
    val chainedTiresRaceCar = ChainedTireDecorator(offRoadRaceCar)
    println("Speed of Chained Tire Decorator: ${chainedTiresRaceCar.getSpeed()}")
    println("Traction of Chained Tire Decorator: ${chainedTiresRaceCar.getTraction()}")
}

