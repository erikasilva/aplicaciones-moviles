interface Book{
    fun getInfo()
    fun order()
}

enum class BookType{
    Science, Literature, Comedy
}

class BookFactory{
    fun create(bookType: BookType) : Book? {
        when(bookType){
            BookType.Science -> return Science()
            BookType.Literature -> return Literature()
            BookType.Comedy ->  return Comedy()
        }
    }
}

class Science : Book{
    private val title = "Theory of relativity"

    override fun getInfo(){
        println("Getting book info: $title ....")
    }

    override fun order(){
        println("Order book: $title")
    }
}

class Literature : Book{
    private val title = "Don Quijote de la Mancha"

    override fun getInfo(){
        println("Getting book info: $title ....")
    }

    override fun order(){
        println("Order book: $title")
    }
}

class Comedy : Book{
    private val title = "La pantera rosa"

    override fun getInfo(){
        println("Getting book info: $title ....")
    }

    override fun order(){
        println("Order book: $title")
    }
}

fun main(){
    val scienceBook = BookFactory().create(BookType.Science)
    scienceBook?.getInfo()
    scienceBook?.order()

    val literatureBook = BookFactory().create(BookType.Literature)
    literatureBook?.getInfo()
    literatureBook?.order()

    val comedyBook = BookFactory().create(BookType.Comedy)
    comedyBook?.getInfo()
    comedyBook?.order()
}